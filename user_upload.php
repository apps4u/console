#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';

// make sure there is enough memory for the file you are processing.
//ini_set('memory_limit', '256M');

use Console\DB\Database;
use Console\File\FileParser;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;
use Symfony\Component\Console\Style\SymfonyStyle;

$helpMessage = <<<EOD

User Upload.

The file must be in current working directory or full file path must be used for
setting the --file option.

set verbosity level for more detailed output.

example:

    Insert Users:
    ./user_upload.php --file users.csv --u dbuser --p dbpass --h dbhost --db dbname
    
    --h option can be left blank and will default to localhost IP:
   ./user_upload.php --file users.csv --u dbuser --p dbpass --db dbname
    
    Dry Run:
    ./user_upload.php --file users.csv --dry_run 

    Create Table:
    ./user_upload.php --create_table --u dbuser --p dbpass --h dbhost --db dbname

EOD;


(new SingleCommandApplication())
    ->setName('Catalyst Test Console Command')
    ->setVersion('1.0.0')
    ->addOption('file', null, InputOption::VALUE_REQUIRED, 'csv file name with users')
    ->addOption('create_table', null, InputOption::VALUE_NONE, 'create the database table')
    ->addOption('dry_run', null, InputOption::VALUE_NONE, 'dry run of file, no database inserts')
    ->addOption('u', 'u', InputOption::VALUE_REQUIRED, 'MySQL username')
    ->addOption('p', 'p', InputOption::VALUE_REQUIRED, 'MySQL password')
    ->addOption('h', null, InputOption::VALUE_REQUIRED, 'MySQL hostname', '127.0.0.1')
    ->addOption('db', 'db', InputOption::VALUE_REQUIRED, 'MySQL database name', null)
    ->setHelp($helpMessage)
    ->setCode(function (InputInterface $input, OutputInterface $output) {

        // nicer output, can format..
        $io = new SymfonyStyle($input, $output);

        // short circuit on invalid option pair...
        if ($input->getOption('create_table') && $input->getOption('dry_run')) {
            $io->error("you can not use options `--create_table` & `--dry_run` at the same time");
            // return early...
            return;
        }
        // short circuit on missing options
        if (!$input->getOption('u') && !$input->getOption('p') && !$input->getOption('dry_run')) {
            $io->error('you must set options --u --p when your not using the --dry_run option');
            // return
            return;
        }
        // short circuit on missing options
        if (!$input->getOption('file') && !$input->getOption('create_table')) {
            $io->error('--file option must be set unless you are running with --create_table option');
            return;
        }

        // if create_table option exists then drop and and create table
        if ($input->getOption('create_table')) {

            // database connection using static class methods of Database:class...
            $conn = Database::openConnection($input->getOption('h'), $input->getOption('u'), $input->getOption('p'), $input->getOption('db'));

            // check returned error..
            if ($conn->error) {
                $io->error("error connecting to database: {$conn->error}");
                // close database connection.
                Database::closeConnection($conn);

                // return early..
                return;
            }

            // drop table first..
            Database::dropTable($conn);
            // create table..
            if (Database::createTable($conn)) {
                // write success in creating table to console.
                $io->success('created table');

                // close database connection.
                Database::closeConnection($conn);

                // return early...
                return;
            } else {
                // in case of error creating table write to console.
                $io->error('unable to create table');

                // close database connection.
                Database::closeConnection($conn);

                // return early...
                return;
            }

        } else {

            // test if file exists if not report error and exit early..
            if (!file_exists($input->getOption('file'))) {
                $io->error("--file dose not exist");
                // exit early..
                return;
            }

            // get line count
            $linecount = count(file($input->getOption('file')));
            // create 2 lines
            $io->newLine(2);
            // create progressbar
            $progress = $io->createProgressBar($linecount);
            // set progressbar to linecount
            $progress->start($linecount);

            // if verbosity is verbose then set custom format on the progress bar.
            if (($io->getVerbosity() >= $io::VERBOSITY_VERBOSE)) {
                // set format showing memory usage.
                $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
            }

            // read file and parse.. passing in progressbar
            $fileParser = new FileParser($input->getOption('file'), $progress);
            // add 2 lines
            $io->newLine(2);

            //to local var for nice console messages..
            $validUser = sizeof($fileParser->getValidUsers());
            $inValidUser = sizeof($fileParser->getInvalidUsers());

            // console output number of users found and how many are valid or not.
            $io->success("total of {$fileParser->totalUsers} users with $validUser valid users & $inValidUser invalid users out of $linecount lines in the file.");

            // if not dry run.
            if (!$input->getOption('dry_run')) {
                // database connection using static class methods of Database:class...
                $conn = Database::openConnection($input->getOption('h'), $input->getOption('u'), $input->getOption('p'), $input->getOption('db'));
                // check returned error..
                if ($conn->error) {
                    $io->error("error connecting to database: {$conn->error}");
                    // close database connection.
                    Database::closeConnection($conn);

                    // return early..
                    return;
                }
                if ($io->getVerbosity() >= $io::VERBOSITY_VERBOSE) {
                    // create 2 lines
                    $io->newLine(2);
                    // if verbosity verbose then create progress using Closure callback
                    $insertProgress = $io->createProgressBar($validUser);

                    $insertProgress->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');

                    // insert into database.
                    $error = Database::insertRows($conn, $fileParser->getValidUsers(), $fileParser->getIndexes(),
                        function (int $advanceInt) use ($insertProgress) {
                            $insertProgress->advance($advanceInt);
                        });
                    // completed insert finnish insertProgressBar
                    $insertProgress->finish();
                    // create 2 lines
                    $io->newLine(2);
                    // if error inserting...
                    if ($error) {
                        $io->error('error generating sql & inserting rows');
                        // database close.
                        Database::closeConnection($conn);
                        // return early..
                        return;
                    }

                } else {

                    // insert into database.
                    if (Database::insertRows($conn, $fileParser->getValidUsers(), $fileParser->getIndexes())) {
                        $io->error('error generating sql & inserting rows');
                        Database::closeConnection($conn);
                        // return early..
                        return;

                    }
                }
                // close database connection
                Database::closeConnection($conn);
                // success number of rows inserted, valid users...
                $io->success("inserted $validUser valid users into table users");
                // create 2 lines
                $io->newLine(1);
                // warning message of number of rows not inserted, invalid users...
                $io->warning("$inValidUser invalid users not inserted to database");
            }

            //Note: output invalid users to console as table even if dry run.
            //output invalid rows from csv file using $io for styled table...

            switch ($io->getVerbosity()) {
                case $io::VERBOSITY_VERBOSE:
                    $io->title('First 20 Invalid Users');
                    $io->table(['name', 'surname', 'email'], array_slice($fileParser->getInvalidUsers(), 0, 20));
                    if ($inValidUser > 20) {
                        $io->writeln('............ ' . $inValidUser - 20 . ' more invalid users ....................');
                    }
                    break;
                case $io::VERBOSITY_VERY_VERBOSE:
                    $io->title('First 50 Invalid Users');
                    $io->table(['name', 'surname', 'email'], array_slice($fileParser->getInvalidUsers(), 0, 50));
                    if ($inValidUser > 50) {
                        $io->writeln('............ ' . $inValidUser - 50 . ' more invalid users ....................');
                    }
                    break;
                case $io::VERBOSITY_DEBUG:
                    $io->title('All Invalid Users');
                    $io->table(['name', 'surname', 'email'], $fileParser->getInvalidUsers());
                    break;
                default:
                    $io->title('First 10 Invalid Users');
                    $io->table(['name', 'surname', 'email'], array_slice($fileParser->getInvalidUsers(), 0, 10));
                    if ($inValidUser > 10) {
                        $io->writeln('............ ' . $inValidUser - 10 . ' more invalid users ....................');
                    }
            }
        }

    })->run();