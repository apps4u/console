# Jason Console Application.

## Install
- first open folder and run `composer install`.
- if needed install `sudo apt-get install php7.2-mysqli -y`
- if `user_upload.php` file is not executable then run `chmod +x user_upload.php`.
- db user will require `create schema permission` unless you set the `--db` commandline option.

## Usage
- run `./user_upload.php --create_table --u dbuser --p dbpass --h dbhost --db dbname`  to create database tables, --db is optional to set the database name or script will create a database called `test_schema`.
- run `./user_upload.php --file users.csv --u dbuser --p dbpass --h dbhost` this will insert all the valid users into the database and return all invalid users to console as table. validation test is on email using regex matching rfc compliant email.
- run `./user_upload.php --file users.csv --dry_run` this will only read the users from csv file and then validate the email address and format all 3 columns, this won't insert any records into the database.
- verbosity level will change the output.

## Mistake
- I had already got too far when I noticed in the pdf that the -u -p -h options should only have 1 dash now I could not do that in my case as `Symfony/Console` package already binds the `-h` option for help, so I could only use `--h`.

- To make up for this I've made the csv reader not care about the order the columns are in so as long as at least 3 columns exists with names of `name`, `surname`, `email` on the first row then this will read and insert the user into the database with the correct columns. 


## Design Plan.

**Database**
- for the database,I'm not going to use a package as I have only 3 sql statements to handle. Create Schema, Drop Schema, Insert Rows.
- I will be using mysqli for my connection, I will create a DB class.
- requires: `sudo apt-get install php7.2-mysqli -y`

**Console Script**
- for the main script I will use a composer package symfony/console as I think this is more of a real world example.

**File Reading**
- for file parsing I will use built in csv functions.
- create a class that I can read & write & validate csv data.


### Changes.
- I've added another commandline option to pass in the database schema name if not the script will also create a new database as it was not in the pdf what name to use for the database.
- I also made sure that the column order was not fixed so as long as the csv has the right column names it dose not matter what order they are in.


