<?php namespace Console\DB;

use Closure;
use mysqli;

/**
 * Created by Jason Kristian.
 * Project: console
 * Author: jasonkristian
 * Date: 6/11/20
 * Time: 9:10 am
 *
 *  Database class is just a namespace for all the database related functions this class contains no state.
 *
 */
class Database
{
    /**
     * @param string $dbhost
     * @param string $dbuser
     * @param string $dbpass
     * @param string|null $dbname
     * @return mysqli
     */
    public static function openConnection(string $dbhost, string $dbuser, string $dbpass, $dbname = null): mysqli
    {
        $conn = null;
        if ($dbname) {
            $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
        } else {
            $conn = new mysqli($dbhost, $dbuser, $dbpass);

            $conn->query('CREATE SCHEMA IF NOT EXISTS `TEST_SCHEMA`;');
            $conn->query('USE `TEST_SCHEMA`;');
        }
        return $conn;
    }

    /**
     * @param mysqli $conn
     */
    public static function closeConnection(mysqli $conn)
    {
        $conn->close();
    }

    /**
     * InsertRows will generate sql to insert into users table, from the rows array and indexes for each row in the $index array.
     * This is safe to use with large arrays, each sql statement will contain no more then 1000 rows per statement.
     *
     * @param mysqli $conn mysqli connection to exec query or queries on.
     * @param array $rows array containing rows to insert into table.
     * @param array $index index of each column to insert
     * @param Closure|null $rowIterationClosure optional callback that returns constant int 1 for each row iterated. this is so I can hookup progress bar without having to pass in progress bar as this should not care about context it used in.
     * @return bool error if true..
     */
    public static function insertRows(mysqli $conn, array $rows, array $index, Closure $rowIterationClosure = null)
    {
        $query = "INSERT INTO `users` (`name`,`surname`, `email`) VALUES";
        $values = '';
        $rowNum = 0;
        $max = 1000;
        $sqlQueries = [];

        $rowsSize = sizeof($rows);

        foreach ($rows as $row) {
            $rowNum++;
            $values .= "({$row[$index[0]]},{$row[$index[1]]}, {$row[$index[2]]}),";

            if ($rowNum === $rowsSize || $rowNum === $max) {
                $sqlQueries[] = self::generateSQL($values, $query);
                // clear out $values
                $values = '';
                $max += 1000;
            }
            if ($rowIterationClosure) {
                $rowIterationClosure(1);
            }
        }
        $queriesResults = [];
        array_map(function ($sql) use ($conn) {
            return $conn->query($sql);
        }, $sqlQueries, $queriesResults);
        // return false if contains false for error..
        return in_array(false, $queriesResults);
    }

    /**
     * @param mysqli $conn
     * @return bool
     */
    public static function createTable(mysqli $conn): bool
    {
        $schema = "create table if not exists `users`(`id` int auto_increment, `name` varchar(100), `surname` varchar(100), `email` varchar(255), UNIQUE KEY(`email`),  primary key(`id`));";
        return $conn->query($schema);
    }

    /**
     * @param mysqli $conn
     * @return bool
     */
    public static function dropTable(mysqli $conn): bool
    {
        $schema = "drop table if exists `users`";
        return $conn->query($schema);
    }

    /**
     * @param string $values
     * @param string $query
     * @return string
     */
    private static function generateSQL(string $values, string $query): string
    {
        // trim remove last `,` from $values
        $values = substr($values, 0, strlen($values) - 1);
        // concat values to query and ignore dupe indexes on email...
        $sql = $query . $values . ' ON DUPLICATE KEY IGNORE';
        return $sql;
    }
}