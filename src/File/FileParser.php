<?php namespace Console\File;


use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Created by Jason Kristian.
 * Project: console
 * Author: jasonkristian
 * Date: 6/11/20
 * Time: 9:14 am
 *
 * This class is a parser for the file but it also a data class holding onto state.. maybe should be two classes and not handled in
 * __construct but for this test it will do while the logic is sound this class and the amount of work I do in the __construct method
 * is by far from perfect but I hope for this test as I was trying to keep it simple and by explaining that here I hope points are not
 * taken off for creating this class like it is. I was just trying to keep the api of this class simple to use within the calling scope.
 *
 */
class FileParser
{
    // csv file of users filename
    private $fileName;
    // total number of users.
    public int $totalUsers;
    // all users with valid email address.
    private $validUsers = [];
    // all users with invalid email address.
    private $invalidUsers = [];
    // indexes are in the order of Name, Surname, Email.
    private $indexes = [];
    // RFC compliant email address regex
    // const EMAIL_REGEX = "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    const EMAIL_REGEX = "/^[-!#$%&\'*+\\.\/0-9=?A-Z^_`{|}~]+@([-0-9A-Z]+\.)+([0-9A-Z]){2,4}$/i";

    /**
     * FileParser constructor.
     * @param string $file
     * @param ProgressBar $progress
     */
    public function __construct(string $file, ProgressBar $progress)
    {
        // store filename no need for it but why not. :)
        $this->fileName = $file;


        // try to open file.
        if (($handle = fopen($this->fileName, "r")) !== false) {
            $i = 0;
            //note: get the index of each column by name so that the order of the columns in csv dose not matter.
            $email = 2;
            $firstName = 0;
            $lastName = 1;


            // while csv has lines...
            while (($data = fgetcsv($handle, ",")) !== false) {
                // empty lines
                if ($data[0] === null) {
                    continue;
                }
                $i++;

                if ($i == 1) {
                    foreach ($data as $key => $value) {
                        if ($value === 'email') {
                            $email = $key;
                        }
                        if ($value === 'name') {
                            $firstName = $key;
                        }
                        if ($value === 'surname') {
                            $lastName = $key;
                        }
                    }
                    // store indexes in known order on class property...
                    $this->indexes = [$firstName, $lastName, $email];
                    continue;
                }

                // if email address is valid using rfc compliant email regex.
                if ($this->validate((string)trim(strtolower($data[$email])))) {
                    // Note: we do not waste cpu on formatting invalid users.
                    $data[$firstName] = ucfirst(trim($data[$firstName]));//uppercase first letter
                    $data[$lastName] = ucfirst(trim($data[$lastName]));//uppercase first letter
                    $data[$email] = strtolower(trim($data[$email]));//lowercase string
                    // write into validUsers property
                    $this->validUsers[] = $data;
                } else {
                    // write into invalidUsers property
                    $this->invalidUsers[] = $data;
                }

                $progress->advance(1);

            }
            // complete progressbar
            $progress->finish();
            // store the number of rows from csv file into totalUsers property minus 1 for column title row.
            $this->totalUsers = $i - 1;
            // close file handle to clean up resources..
            fclose($handle);
        }
    }

    /**
     * Validate the email address for each row.
     * @param string $row
     * @return bool
     */
    private function validate(string $email): bool
    {
        //return filter_var($email, FILTER_VALIDATE_EMAIL);
        return (preg_match(self::EMAIL_REGEX, $email) === 1) ? true : false;
    }

    /**
     * Get all Valid users
     *
     * @return array
     */
    public function getValidUsers(): array
    {
        return $this->validUsers;
    }

    /**
     * Get all Invalid users.
     *
     * @return array
     */
    public function getInvalidUsers(): array
    {
        return $this->invalidUsers;
    }

    /**
     * @return array|int[]
     */
    public function getIndexes()
    {
        return $this->indexes;
    }


}