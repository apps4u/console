#!/usr/bin/env php
<?php
/**
 * Created by Apps 4 U Pty Ltd.
 * User: Jason Kristian <jasonkristian@gmail.com>
 * Date: 6/11/20
 * Time: 9:33 pm
 *  © 2020 Apps 4 U Pty. Ltd.
 *
 */
foreach (range(1, 100) as $num) {

    echo $num . ', ';
    if ($num % 3 === 0) {
        echo 'foo, ';
    }
    if ($num % 5 === 0) {
        if ($num === 100) {
            echo 'bar';
        } else {
            echo 'bar, ';
        }

    }
    if (($num % 3 === 0) && ($num % 5 === 0)) {
        echo 'foobar, ';
    }

}